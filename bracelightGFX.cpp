#include "bracelightGFX.h"
#include "fonts/font8x8.h"
IS31FL3731 matrix = IS31FL3731();

uint8_t GAMMALUT[3][1024];
bracelightGFX::bracelightGFX(){
}


void bracelightGFX::initialize(TwoWire * thisWire){

  pinMode(RES_DIV_GND, OUTPUT);
  digitalWrite(RES_DIV_GND, LOW);
  pinMode(HALF_BV, INPUT);
  pinMode(IR_PWR, OUTPUT);
  pinMode(IR_SEND, OUTPUT);
  digitalWrite(IR_PWR, LOW);
  digitalWrite(IR_SEND, LOW);
  pinMode(ACC_CS, OUTPUT);
  digitalWrite(ACC_CS, HIGH);
  pinMode(ACC_INT1, INPUT);
  pinMode(IS_INTB, INPUT);
  
  pinMode(IS_SHUTDOWN_PIN, OUTPUT);
  digitalWrite(IS_SHUTDOWN_PIN, HIGH);
  delay(20);
  pinMode(IS_SHUTDOWN_PIN, OUTPUT);
  digitalWrite(IS_SHUTDOWN_PIN, LOW);
  delay(20);
  pinMode(IS_SHUTDOWN_PIN, OUTPUT);
  digitalWrite(IS_SHUTDOWN_PIN, HIGH);
  delay(20);
  
  SerialUSB.println("ISSI manual animation test");
  if (! matrix.begin(thisWire)) {
    SerialUSB.println("IS31 not found");
    while (1);
  }
  SerialUSB.println("IS31 Found!");

  // turn on LEDs
  // turn off blink
  for(int i = 0; i < 0x12; i++){
    LED_ON[i] = 0xFF;
  }
  blinkOff();

  for(int i = 0; i < LEDCOUNT; i++){
    // turn on the applicable LEDs
    LED_ON[ADDR[i][RED]/8] |= (1 << (ADDR[i][RED]%8));
    LED_ON[ADDR[i][BLU]/8] |= (1 << (ADDR[i][BLU]%8));
    LED_ON[ADDR[i][GRN]/8] |= (1 << (ADDR[i][GRN]%8));
  }
  matrix.writeOnMatrix(LED_ON);

  
  for(int i = 0; i < 1024; i++){
    GAMMALUT[RED][i] = pow(i/1024.0, GAMMA[RED])*255;
    GAMMALUT[GRN][i] = pow(i/1024.0, GAMMA[GRN])*255;
    GAMMALUT[BLU][i] = pow(i/1024.0, GAMMA[BLU])*255;
  }
}


inline void bracelightGFX::loadSingleLed(uint8_t * rgb, uint8_t LEDNO){
    brightnessVector[ADDR[LEDNO][RED]] = rgb[RED];
    brightnessVector[ADDR[LEDNO][GRN]] = rgb[GRN];
    brightnessVector[ADDR[LEDNO][BLU]] = rgb[BLU];
}

void bracelightGFX::loadSingleLedColor(uint8_t color, uint8_t LEDNO){
    colorToRgb(color, rgb);
    loadSingleLed(rgb, LEDNO);
}

void bracelightGFX::loadSingleLedHSV(double hue, double saturation, double value, uint8_t LEDNO){
    hsvToRgb(hue, saturation, value, GAMMA, rgb);
    loadSingleLed(rgb, LEDNO);
}

void bracelightGFX::pattern_rainbow(double phase, uint8_t startled, uint8_t endled, double alpha){
    for(int i = startled; i < endled; i++){
      
      phase += alpha;
      if(phase>=1)
        phase-=1;
      // write rainbow pattern
      loadSingleLedHSV(phase, MAXSATURATION, brightness, i);
    }
}
  
void bracelightGFX::pattern_valuefade2(double phase, uint8_t startled, uint8_t endled, double alpha, uint8_t * customColor){
    for(int i = startled; i < endled; i++){
      phase += alpha;
      if(phase>=1)
        phase-=1;
        
      double adjPhase[2] = {(sin(phase*2*PI)+1)/2, 1-(sin(phase*2*PI)+1)/2};
      uint8_t colorSum[3] = {0,0,0};
      
      for(int j = 0; j < 2; j++){
        colorToRgb(customColor[j], rgb);
        colorSum[0] += (rgb[RED])*adjPhase[j];
        colorSum[1] += (rgb[GRN])*adjPhase[j];
        colorSum[2] += (rgb[BLU])*adjPhase[j];
      }
      
      loadSingleLed(colorSum, i);
    }
}
  

void bracelightGFX::pattern_huefade2(double phase, uint8_t startled, uint8_t endled, double alpha, uint8_t * customColor){
    for(int i = startled; i < endled; i++){
      phase += alpha;
      if(phase>=1)
        phase-=1;
        
      double adjPhase[2] = {(sin(phase*2*PI)+1)/2, 1-((sin(phase*2*PI)+1)/2)};
      hsvToRgb(colorHue[customColor[0]]*adjPhase[0] + colorHue[customColor[1]]*adjPhase[1], 
      colorSaturation[customColor[0]]*adjPhase[0] + colorSaturation[customColor[1]]*adjPhase[1], 
      (colorValue[customColor[0]]*adjPhase[0] + colorValue[customColor[1]]*adjPhase[1])*brightness, 
      GAMMA, rgb);
      
      loadSingleLed(rgb, i);
    }
}
  
void bracelightGFX::pattern_swap3(double phase, uint8_t startled, uint8_t endled, double alpha, uint8_t * customColor){
    for(int i = startled; i < endled; i++){
      phase += alpha;
      if(phase>=1)
        phase-=1;
      colorToRgb(customColor[(int)(phase*3)], rgb);
      loadSingleLed(rgb, i);
    }
}

void bracelightGFX::pattern_fill(uint8_t color, uint8_t startled, uint8_t endled){
    for(int i = startled; i < endled; i++){
      loadSingleLedColor(color, i);
    }
}

void bracelightGFX::pattern_text(uint8_t phase, uint8_t startled, String disptext){
  // to keep the LED switching time of this pattern consistent,
  // we will be using movie mode. 

  for(int i = 0; i < 8; i++){
    if(font8x8_basic[disptext[phase/8]][i] & (1<<(phase%8)))
      loadSingleLed(rgbWhite, startled + i);
    else
      loadSingleLed(rgbBlack, startled + i);
  }
    
}

void bracelightGFX::pattern_bitmap(uint8_t startled, uint8_t endled){

  //if(matrix.readRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_CONFIG) != ISSI_REG_CONFIG_AUTOPLAYMODE){
    for(int frame = 0; frame < 8; frame++){
      matrix.setFrame(frame);
      for(int i = startled; i < endled; i++){
        switch((i*(frame+1)/(8*6))%3){
          case(0):
            loadSingleLed(rgbRed, i);
            break;
          case(1):
            loadSingleLed(rgbGreen, i);
            break;
          case(2):
            loadSingleLed(rgbBlue, i);
            break;
        }

        /*
        if(i > frame*3)
          loadSingleLed(rgbBlue, i);
        else
          loadSingleLed(rgbRed, i);*/
          
          
      }
      writeBrightnessArray();
    }
  //}

  autoMode();
  delay(170);
  //while(digitalRead(IS_INTB));
  
  matrix.setFrame(0);
  matrix.displayFrame(0);
  pictureMode();
}

// utility function to quickly calculate gamma adjustments
inline double fastPow(double a, double b) {
  union {
    double d;
    int x[2];
  } u = { a };
  u.x[1] = (int)(b * (u.x[1] - 1072632447) + 1072632447);
  u.x[0] = 0;
  return u.d;
}

/**
 * Converts an HSV color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes h, s, and v are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue
 * @param   Number  s       The MAXSATURATION
 * @param   Number  v       The value
 * @return  Array           The RGB representation
 */
void bracelightGFX::hsvToRgb(double h, double s, double v, double * gamma, uint8_t * rgb) {
    double r, g, b;
    
    int i = int(h * 6);
    double f = h * 6 - i;
    double p = v * (1 - s);
    double q = v * (1 - f * s);
    double t = v * (1 - (1 - f) * s);

    switch(i % 6){
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }

    // store to bV with gamma correction
    /*
    rgb[RED] = fastPow(r, 1/gamma[RED]) * colorAdjustment[RED];
    rgb[GRN] = fastPow(g, 1/gamma[GRN]) * colorAdjustment[GRN];
    rgb[BLU] = fastPow(b, 1/gamma[BLU]) * colorAdjustment[BLU];*/


    rgb[RED] = GAMMALUT[RED][(int)ldexp(r, 10)];
    rgb[GRN] = GAMMALUT[GRN][(int)ldexp(g, 10)];
    rgb[BLU] = GAMMALUT[BLU][(int)ldexp(b, 10)];
    
}

void bracelightGFX::colorToRgb(uint8_t color, uint8_t * rgb){
  hsvToRgb(colorHue[color], colorSaturation[color], colorValue[color]*brightness, GAMMA, rgb);
}


void bracelightGFX::allLedsOn(void){
  for(int i = 0; i < 0x12; i++)
    LED_ON[i] = 0x00;
  for(int i = 0; i < LEDCOUNT; i++){
    // turn on the applicable LEDs
    //if(random(15) == 0){
      LED_ON[ADDR[i][RED]/8] |= (1 << (ADDR[i][RED]%8));
      LED_ON[ADDR[i][BLU]/8] |= (1 << (ADDR[i][BLU]%8));
      LED_ON[ADDR[i][GRN]/8] |= (1 << (ADDR[i][GRN]%8));
    //}
  }
}

uint8_t currFrame = 0;

void bracelightGFX::writeBrightnessArray(void){
    matrix.writeOnMatrix(LED_ON);
    blinkOff();
    matrix.writeVector(brightnessVector, 0, 107);
}


void bracelightGFX::writeBrightnessArrayQuick(void){
    matrix.writeVector(brightnessVector, 0, 107);
}

void bracelightGFX::writeBlinkMatrix(uint8_t * blinkPattern){
  matrix.writeBlinkMatrix(blinkPattern);
}

void bracelightGFX::blinkOneLED(uint8_t LEDNO){
  for(int i = 0; i < 0x12; i++)
    LED_BLINK[i] = 0x00;
  LED_BLINK[ADDR[LEDNO][RED]/8] |= (1 << (ADDR[LEDNO][RED]%8));
  LED_BLINK[ADDR[LEDNO][BLU]/8] |= (1 << (ADDR[LEDNO][BLU]%8));
  LED_BLINK[ADDR[LEDNO][GRN]/8] |= (1 << (ADDR[LEDNO][GRN]%8));
  matrix.writeBlinkMatrix(LED_BLINK);
}

void bracelightGFX::blinkOff(void){
  for(int i = 0; i < 0x12; i++)
    LED_BLINK[i] = 0x00;
  matrix.writeBlinkMatrix(LED_BLINK);
}

void bracelightGFX::setBrightness(double thisbrightness){
  brightness = thisbrightness;
}


void bracelightGFX::pictureMode(void){
  matrix.writeRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_CONFIG, ISSI_REG_CONFIG_PICTUREMODE);
}


void bracelightGFX::autoMode(void){
    // auto mode
    matrix.writeRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_CONFIG, ISSI_REG_CONFIG_AUTOPLAYMODE);
    matrix.writeRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_AUTOCONT1, ISSI_REG_AUTOCONT1_ENDLESS_ALLFRAMES);
    matrix.writeRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_AUTOCONT2, ISSI_REG_AUTOCONT2_11ms);
}

