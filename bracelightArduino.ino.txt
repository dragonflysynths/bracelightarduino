#include <ArduinoLowPower.h>
#include "OneButton.h"
#include <Arduino.h>
#include "wiring_private.h" // pinPeripheral() function
#include <Wire.h>
#include "Tiny9_LIS2HH12.h"
#include "bracelightGFX.h"


#define BUTTONPIN 20
#define RES_DIV_GND 9
#define HALF_BV     8 
#define IR_PWR 18
#define IR_SEND 30
#define ACC_CS 12
#define ACC_INT1 6
#define ACC_INT2 7

// extra wire on these pins
#define myPA12 22
#define myPA13 38
// pins PA12 and PA13
TwoWire myWire(&sercom2, myPA12, myPA13);


#define STATE_RAINBOW 0
#define STATE_FADE    1
#define STATE_HUEFADE 2
#define STATE_SWAP    3
#define STATE_OFF     4
#define STATE_OFF2    5
#define STATE_COUNT   6
const double speed_tauAdj[STATE_COUNT]  = {.1, .1, .1, .1, .1, .1};
const double lengthAdj[STATE_COUNT] = {.1, .1, .1, .1, .1, .1};

#define FSM_DISPLAY 0
#define FSM_SHOW_PARAM_COLOR 1
#define FSM_CTRL_COLOR 2
#define FSM_CTRL_SPEED 3
#define FSM_CTRL_LENGTH 4

uint8_t state_display = 0;
uint8_t state_ctrl = 0, FSM = FSM_DISPLAY, colorSelector;

void myClickFunction(void);
void myDoubleClickFunction(void);
void myLongPressStartFunction(void);
void myLongPressStopFunction(void);

// Setup a new OneButton on pin A1.  
OneButton button(BUTTONPIN, true);

LIS2HH12 lis = LIS2HH12(&myWire);

int gravity = 0; //+/-2g = 0, +/- 4g = 2, +/- 8g = 3; 1 is not available
int x,y,z; 
float diff = 0.00;
int major = 0;
int minor = 0;
float x2, y2, z2;

#define VARIANTCOUNT 3
#define CUSTOMCOLORCOUNT 3
uint8_t customColor[VARIANTCOUNT][CUSTOMCOLORCOUNT] = {{BL_RED, BL_WHITE, BL_BLUE}, {BL_BLUE, BL_RED, BL_PURPLE}, {BL_GREEN, BL_BLACK, BL_BLACK}};
uint8_t currVariant = 0;

void setup()
{
  SerialUSB.begin(115200);
  //while(!SerialUSB);
  delay(1000);

  pinMode(RES_DIV_GND, OUTPUT);
  digitalWrite(RES_DIV_GND, LOW);
  pinMode(HALF_BV, INPUT);
  pinMode(IS_SHUTDOWN_PIN, OUTPUT);
  digitalWrite(IS_SHUTDOWN_PIN, HIGH);
  pinMode(IR_PWR, OUTPUT);
  pinMode(IR_SEND, OUTPUT);
  digitalWrite(IR_PWR, LOW);
  digitalWrite(IR_SEND, LOW);
  pinMode(ACC_CS, OUTPUT);
  digitalWrite(ACC_CS, HIGH);

  // start up i2c
  myWire.begin();
  myWire.setClock(400000);
  // Assign pins 22 & 38 to SERCOM functionality
  pinPeripheral(myPA12, PIO_SERCOM);
  pinPeripheral(myPA13, PIO_SERCOM);
  
  // link the myClickFunction function to be called on a click event.   
  button.attachClick(myClickFunction);
  button.setClickTicks(300);

  // link the doubleclick function to be called on a doubleclick event.   
  button.attachDoubleClick(myDoubleClickFunction);

  button.attachLongPressStart(myLongPressStartFunction);
  button.attachSuperLongPressStart(mySuperLongPressStartFunction);
  button.attachUltraLongPressStart(myUltraLongPressStartFunction);
  button.attachLongPressStop(myLongPressStopFunction);
  button.attachSuperLongPressStop(mySuperLongPressStopFunction);
  button.attachUltraLongPressStop(myUltraLongPressStopFunction);
  
  // set 40 msec. debouncing time. Default is 50 msec.
  button.setDebounceTicks(40);

  initGFX();
  
  lis.powerOn();

  SerialUSB.println(lis.readRegister8(LIS2HH12_WHO_AM_I));
  SerialUSB.println(lis.WHOAMI());

  // set frequency 100Hz
  lis.writeRegister8(LIS2HH12_CTRL1, 0b00110111);
  // enable interrupt 1 on INT1 line
  lis.writeRegister8(LIS2HH12_CTRL3, 0b00001000);
  // enable all said interrupts (6D, XL, XH ... ZH)
  lis.writeRegister8(LIS2HH12_IG_CFG1, 0b00001000);
  // set threshold registers to arbitrary value
  lis.writeRegister8(LIS2HH12_IG_THS_X1, 0b00100000);
  lis.writeRegister8(LIS2HH12_IG_THS_Y1, 0b00100000);
  lis.writeRegister8(LIS2HH12_IG_THS_Z1, 0b00100000);
  // set duration to some value
  lis.writeRegister8(LIS2HH12_IG_DUR1, 0b00000001);
  // attach interrupt to line ACC_INT1
  attachInterrupt(ACC_INT1, movementInterrupt, RISING);
}

volatile bool mvmtInterrupt = false;
void movementInterrupt(void){
  mvmtInterrupt = true;
}


void(* resetFunc) (void) = 0; //declare reset function @ address 0

double phase = 0, speed_tau = .5, length_tau = .5;
boolean newState = false;
boolean phaseReset = false;
int maxVoltsLED;
double motionBrightness;

void updateImage(uint8_t frame){
  uint8_t startled = FIRSTLED + frame;
  uint8_t endled   = LEDCOUNT - frame;
  
  switch(state_display){
  case STATE_RAINBOW:
    pattern_rainbow(phase, startled, endled, length_tau*lengthAdj[state_display]);
  break;
  
  case STATE_FADE:
    pattern_valuefade2(phase, startled, endled, length_tau*lengthAdj[state_display], customColor[currVariant]);
  break;
  
  case STATE_HUEFADE:
    pattern_huefade2(phase, startled, endled, length_tau*lengthAdj[state_display], customColor[currVariant]);
  break;
  
  case STATE_SWAP:
    pattern_swap3(phase, startled, endled, length_tau*lengthAdj[state_display], customColor[currVariant]);
  break;
  
  case STATE_OFF:
    SerialUSB.println("SLEEP!!");
    // disable usb
    USBDevice.detach();
    //turn off led chip
    digitalWrite(IS_SHUTDOWN_PIN, LOW);
    //turn off ACC
    lis.writeRegister8(LIS2HH12_CTRL1, 0b00000000);
    // wait for button release
    while(!digitalRead(BUTTONPIN));
    // and debounce
    delay(20);

    // wakeup on button press
    LowPower.attachInterruptWakeup(BUTTONPIN, NULL, CHANGE);
    LowPower.sleep();

    // wakeup here
    USBDevice.attach();
    detachInterrupt(BUTTONPIN);
    SerialUSB.println("Waking up");
    // wait for button release
    while(!digitalRead(BUTTONPIN));
    // and debounce
    delay(20);

    // turn chip back on
    digitalWrite(IS_SHUTDOWN_PIN, HIGH);
    // turn ACC on, 100Hz
    lis.writeRegister8(LIS2HH12_CTRL1, 0b00110111);
    // increase color variant
    currVariant = (currVariant +1) % VARIANTCOUNT;
    
    // display battery voltage
    delay(20);
    digitalWrite(RES_DIV_GND, LOW);
    delay(20);
    //maxVoltsLED = analogRead(HALF_BV); //  * (endled - startled) / 1024;
    delay(20);
    digitalWrite(RES_DIV_GND, HIGH);
    delay(20);
    pattern_fill(RED, startled, endled);
    
    // move to next state
    state_display = STATE_OFF2;
    //SerialUSB.println("WAKE!!");
  break;
  
  case STATE_OFF2:
    delay(250);
    state_display = STATE_RAINBOW;
  break;
  
  default:
  
  break;
  }
    
  newState = false;
  phaseReset = false;
  
  // always increase phase, for simplicity
  // with dither
  double dither = random(-1000, 1000) / 100000.0;
  phase += (speed_tau*speed_tauAdj[state_display]) + dither;
  if(phase>=1){
    phase -= 1;
    phaseReset = true;
  }
}


void loop()
{
  uint8_t rgb[3];
  uint8_t csAddr;

  // if there is a mvmt interrupt
  if(mvmtInterrupt){
    // for now, run 1 simple pattern
    pattern_fill(BL_CYAN, FIRSTLED, LEDCOUNT-1);
    delay(500);
  }
  mvmtInterrupt = false;
  
  // keep watching the push button:
  button.tick();

  switch(FSM){
  case(FSM_DISPLAY):
    updateImage(0);
  break;

  case(FSM_SHOW_PARAM_COLOR):
    pattern_fill(state_ctrl*2, FIRSTLED, LEDCOUNT-1);

  break;

  case(FSM_CTRL_COLOR):
    // zero everything out
    for(int i = 0; i < LEDCOUNT; i++){
      loadSingleLedHSV(0, MAXSATURATION, 0, i);
    }
    
    // leave a gap between variants (+1)
    for(int i = 0; i < VARIANTCOUNT; i++){
      for(int j = 0; j < CUSTOMCOLORCOUNT; j++){
        loadSingleLedColor(customColor[i][j], j + i*(CUSTOMCOLORCOUNT+1));
      }
    }
    
    // make the active color blink
    for(int i = 0; i < 0x12; i++)
      BLINKMATRIX[i] = 0x00;
    // turn on the applicable RGB LED
    // (adjust for skipped led)
    csAddr = colorSelector + (colorSelector/3);
    BLINKMATRIX[ADDR[csAddr][RED]/8] |= (1 << (ADDR[csAddr][RED]%8));
    BLINKMATRIX[ADDR[csAddr][BLU]/8] |= (1 << (ADDR[csAddr][BLU]%8));
    BLINKMATRIX[ADDR[csAddr][GRN]/8] |= (1 << (ADDR[csAddr][GRN]%8));
    writeBlinkMatrix(BLINKMATRIX);

  break;

  case(FSM_CTRL_SPEED): 
    updateImage(5);
  break;

  case(FSM_CTRL_LENGTH):
    updateImage(5);
  break;

  }

  allLedsOn();
  
  lis.readAccel(&x, &y, &z); 
  scale();
  SerialUSB.println(x2);
  motionBrightness = abs(x2);
  if(motionBrightness>1)
    motionBrightness = 1;
  writeBrightnessArray();
  delay(15);
}


#define FSM_DISPLAY 0
#define FSM_SHOW_PARAM_COLOR 1
#define FSM_CTRL_COLOR 2
#define FSM_CTRL_SPEED 3
#define FSM_CTRL_LENGTH 4


// this function will be called when the button was pressed 1 time and them some time has passed.
void myClickFunction() {

  //SerialUSB.println("CLICK");
  
  switch(FSM){
  case(FSM_DISPLAY):
    state_display = (state_display + 1) % STATE_COUNT;
    phase = 0;
    newState = true;
  break;

  case(FSM_SHOW_PARAM_COLOR):
    // should be impossible. return to regular display
    FSM = FSM_DISPLAY;
    button.purgatory();

  case(FSM_CTRL_COLOR):
  // from color state_display, short press changes color
  customColor[colorSelector/3][colorSelector%3] = (customColor[colorSelector/3][colorSelector%3]+1) % UNIQUECOLORS;
  break;
  
  case(FSM_CTRL_SPEED):
  // change speed_tau
  speed_tau = speed_tau + .1;
  if(speed_tau > 1)
    speed_tau = .1;
  break;
  
  
  case(FSM_CTRL_LENGTH):
  // change length_tau
  length_tau = length_tau + .1;
  if(length_tau > 1)
    length_tau = 0;
  break;
  }
} // myClickFunction


// this function will be called when the button was pressed 2 times in a short timeframe.
void myDoubleClickFunction() {
  SerialUSB.println("DOUBLE CLICK");
  if(FSM == FSM_DISPLAY){
    // undo state_display change from single click
    state_display = (state_display - 1) % STATE_COUNT;
    
    brightness = (brightness + .1);
    if(brightness >= 0.7)
      brightness = .2;
    brightnessGamma = pow(brightness, 1/GAMMA);
  }
} // myDoubleClickFunction


#define STATE_MENU_COLOR 0 
#define STATE_MENU_SPEED 1
#define STATE_MENU_LENGTH 2

// this function will be called when the button was pressed 1 time and them some time has passed.
// long press corresponds to color selector
// display the color selector frame, but only move to that FSM on release (STOP function)
void myLongPressStartFunction() {

  SerialUSB.println("LONG START");
  switch(FSM){
  case(FSM_DISPLAY):
  // if coming from display FSM
  FSM = FSM_SHOW_PARAM_COLOR;
  state_ctrl = STATE_MENU_COLOR;
  break;

  case(FSM_SHOW_PARAM_COLOR):
  // should be impossible. return to regular display
  FSM = FSM_DISPLAY;
  button.purgatory();

  case(FSM_CTRL_COLOR):
    // from color state_display, long press changes color location
    colorSelector = (colorSelector + 1)%9;
  break;
  
  case(FSM_CTRL_SPEED):
  // exit mode
  FSM = FSM_DISPLAY;
  button.purgatory();
  
  case(FSM_CTRL_LENGTH):
  // exit mode
  FSM = FSM_DISPLAY;
  button.purgatory();
  break;
  }
}

void myLongPressStopFunction() {
  SerialUSB.println("LONG STOP");
  // move to the actual control FSM if coming from display
  if(FSM == FSM_SHOW_PARAM_COLOR){
    FSM = FSM_CTRL_COLOR;
    colorSelector = 0;
    
  }
}

// super long press corresponds to speed_tau adjustment, or return to display FSM
void mySuperLongPressStartFunction() {
  
  SerialUSB.println("S LONG START");
  // move to the actual control FSM if coming from display
  if(FSM == FSM_SHOW_PARAM_COLOR){
    state_ctrl = STATE_MENU_SPEED;
  }
  else{
    FSM = FSM_DISPLAY;
    button.purgatory();
  }
  
}

void mySuperLongPressStopFunction() {
  SerialUSB.println("S LONG STOP");
  // move to speed_tau control if coming from display
  if(FSM == FSM_SHOW_PARAM_COLOR){
    FSM = FSM_CTRL_SPEED;
  }
  else{
    
    // end blink
    for(int i = 0; i < 0x12; i++)
      BLINKMATRIX[i] = 0x00;
    writeBlinkMatrix(BLINKMATRIX);
    
    FSM = FSM_DISPLAY;
    button.purgatory();
  }
}


// ultra long press corresponds to length_tau adjustment
void myUltraLongPressStartFunction() {
  SerialUSB.println("U LONG START");
  state_ctrl = STATE_MENU_LENGTH;
  button.purgatory();
} 

void myUltraLongPressStopFunction() {
  SerialUSB.println("U LONG STOP");
  FSM = FSM_CTRL_LENGTH;
}

void scale(){
  if(gravity == 2){
      x2 = (float)x*0.000244;
      y2 = (float)y*0.000244;
      z2 = (float)z*0.000244;
  }
  else if(gravity == 1){
      x2 = (float)x*0.000122;
      y2 = (float)y*0.000122;
      z2 = (float)z*0.000122;
  }
  else{
      x2 = (float)x*0.000061;
      y2 = (float)y*0.000061;
      z2 = (float)z*0.000061-1.000;    
  }  
}
