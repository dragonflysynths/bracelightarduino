#include "ArduinoLowPower.h"
#include "OneButton.h"
#include <Arduino.h>
#include "wiring_private.h" // pinPeripheral() function
#include <Wire.h>
#include "Tiny9_LIS2HH12.h"
#include "bracelightGFX.h"



// extra wire on these pins
#define myPA12 22
#define myPA13 38
// pins PA12 and PA13
TwoWire myWire(&sercom2, myPA12, myPA13);


#define STATE_RAINBOW 0
#define STATE_FADE    1
#define STATE_HUEFADE 2
#define STATE_SWAP    3
#define STATE_OFF     4
#define STATE_OFF2    5
#define STATE_COUNT   6
const double speed_tauAdj[STATE_COUNT]  = {.01, .01, .01, .01, .01, .01};
const double lengthAdj[STATE_COUNT] = {.05, .05, .05, .05, .05, .05};

#define FSM_DISPLAY 0
#define FSM_SHOW_PARAM_COLOR 1
#define FSM_CTRL_COLOR 2
#define FSM_CTRL_SPEED 3
#define FSM_CTRL_LENGTH 4

uint8_t state_display = 0;
uint8_t state_ctrl = 0, FSM = FSM_DISPLAY, colorSelector;

void myClickFunction(void);
void myDoubleClickFunction(void);
void myLongPressStartFunction(void);
void myLongPressStopFunction(void);

// Setup a new OneButton on pin A1.  
OneButton button(BUTTONPIN, true);

LIS2HH12 lis = LIS2HH12(&myWire);
bracelightGFX bgfx = bracelightGFX();

int gravity = 0; //+/-2g = 0, +/- 4g = 2, +/- 8g = 3; 1 is not available
int x,y,z; 
float diff = 0.00;
int major = 0;
int minor = 0;
float x2, y2, z2;

#define VARIANTCOUNT 3
#define CUSTOMCOLORCOUNT 3
uint8_t customColor[VARIANTCOUNT][CUSTOMCOLORCOUNT] = {{BL_RED, BL_WHITE, BL_BLUE}, {BL_BLUE, BL_RED, BL_PURPLE}, {BL_GREEN, BL_BLACK, BL_BLACK}};
uint8_t currVariant = 0;

double brightness = .4;
long startTime = 0;
uint8_t BLINKMATRIX[0x12];

void b2s(byte myByte, char * bits){
 byte mask = 0b10000000;
 for(int i = 0; i<8; i++){
   bits[i]='0';
   if(((mask >> i) & myByte) == (mask>>i)){
     bits[i]='1';
   }
 }
 // terminate the string with the null character
 bits[8] = '\0';
}

volatile bool mvmtInterrupt = false;

void setup()
{
  SerialUSB.begin(115200);


  // start up i2c
  myWire.begin();
  myWire.setClock(400000);
  // Assign pins 22 & 38 to SERCOM functionality
  pinPeripheral(myPA12, PIO_SERCOM);
  pinPeripheral(myPA13, PIO_SERCOM);

  bgfx.initialize(&myWire);
  bgfx.setBrightness(brightness);
  
  // link the myClickFunction function to be called on a click event.   
  button.attachClick(myClickFunction);
  button.setClickTicks(300);

  // link the doubleclick function to be called on a doubleclick event.   
  button.attachDoubleClick(myDoubleClickFunction);

  button.attachLongPressStart(myLongPressStartFunction);
  button.attachSuperLongPressStart(mySuperLongPressStartFunction);
  button.attachUltraLongPressStart(myUltraLongPressStartFunction);
  button.attachLongPressStop(myLongPressStopFunction);
  button.attachSuperLongPressStop(mySuperLongPressStopFunction);
  button.attachUltraLongPressStop(myUltraLongPressStopFunction);
  
  // set 40 msec. debouncing time. Default is 50 msec.
  button.setDebounceTicks(40);
  
  lis.powerOn();
  
  // attach interrupt to line ACC_INT1
   attachInterrupt(ACC_INT1, movementInterrupt, CHANGE);
  

  char bits[9];
  int j;

  
  /*while(true){
      b2s(lis.readRegister8(LIS2HH12_IG_SRC1), bits);
      SerialUSB.println(bits);
      //lis.writeRegister8(LIS2HH12_IG_SRC1, 0x00);
      //b2s(lis.readRegister8(LIS2HH12_IG_SRC2), bits);
      //SerialUSB.println(bits);
      //if(lis.readRegister8(LIS2HH12_IG_SRC1) & 2)
        //SerialUSB.println(j++);
      if(mvmtInterrupt){
        SerialUSB.println("MVMT!!!!!!!!!");
        mvmtInterrupt = false;
      }else
        SerialUSB.println(lis.WHOAMI());
      
      delay(100);
  }*/
}

void movementInterrupt(void){
  mvmtInterrupt = true;
}


void(* resetFunc) (void) = 0; //declare reset function @ address 0

double phase = 0, speed_tau = .5, length_tau = .3;
boolean newState = false;
boolean phaseReset = false;
int maxVoltsLED;
double motionBrightness;

void updateImage(uint8_t frame){
  uint8_t startled = FIRSTLED + frame;
  uint8_t endled   = LEDCOUNT - frame;
  
  switch(state_display){
  case STATE_RAINBOW:
    bgfx.pattern_rainbow(phase, startled, endled, length_tau*lengthAdj[state_display]);
  break;
  
  case STATE_FADE:
    bgfx.pattern_valuefade2(phase, startled, endled, length_tau*lengthAdj[state_display], customColor[currVariant]);
  break;
  
  case STATE_HUEFADE:
   bgfx.pattern_huefade2(phase, startled, endled, length_tau*lengthAdj[state_display], customColor[currVariant]);
  break;
  
  case STATE_SWAP:
    bgfx.pattern_swap3(phase, startled, endled, length_tau*lengthAdj[state_display], customColor[currVariant]);
  break;
  
  case STATE_OFF:
    detachInterrupt(ACC_INT1);
    
    SerialUSB.println("SLEEP!!");
    // disable usb
    //USBDevice.detach();
    //turn off led chip
    digitalWrite(IS_SHUTDOWN_PIN, LOW);
    //turn off ACC
    //lis.writeRegister8(LIS2HH12_CTRL1, 0b00000000);
    // wait for button release
    while(!digitalRead(BUTTONPIN));
    // wakeup on button press
    //LowPower.attachInterruptWakeup(BUTTONPIN, NULL, FALLING);
    // and debounce
    delay(50);
    SerialUSB.println(LowPower.attachInterruptWakeup(BUTTONPIN, movementInterrupt, FALLING));
    
    LowPower.sleep();

    // wakeup here
    detachInterrupt(BUTTONPIN);
    //USBDevice.attach();
    //SerialUSB.println("Waking up");
    // wait for button release
    while(!digitalRead(BUTTONPIN));
    // and debounce
    delay(20);

    // turn ACC on, 100Hz
    //lis.writeRegister8(LIS2HH12_CTRL1, 0b00110111);
    // increase color variant
    currVariant = (currVariant +1) % VARIANTCOUNT;

    /*
    // display battery voltage
    delay(20);
    digitalWrite(RES_DIV_GND, LOW);
    delay(20);
    //maxVoltsLED = analogRead(HALF_BV); //  * (endled - startled) / 1024;
    delay(20);
    digitalWrite(RES_DIV_GND, HIGH);
    delay(20);
    bgfx.pattern_fill(BL_BLACK, startled, endled);*/
    
    // move to next state
    state_display = STATE_OFF2;
    //SerialUSB.println("WAKE!!");
    
  break;
  
  case STATE_OFF2:
    // turn chip back on
    digitalWrite(IS_SHUTDOWN_PIN, HIGH);
    //delay(250);
    //attachInterrupt(ACC_INT1, movementInterrupt, RISING);
    attachInterrupt(ACC_INT1, movementInterrupt, CHANGE);
    state_display = STATE_RAINBOW;
  break;
  
  default:
  
  break;
  }
    
  newState = false;
  phaseReset = false;
  
  // always increase phase, for simplicity
  // with dither scaled to speed rate
  double dither = random(-1000, 1000) / 100000.0;
  phase += speed_tau*(speed_tauAdj[state_display] + dither);
  if(phase>=1){
    phase -= 1;
    phaseReset = true;
  }
}

  #define FRAMERATE_HZ 120
  #define FRAMEPERIOD_SECONDS (1.0/FRAMERATE_HZ)

  long lastMvmt = 0;
  uint8_t stringIndex = 0;
  String message[4] = {"fuck", "fuck ", "fuck", "fuck"};
void loop()
  {
    
  //SerialUSB.println(((int)lis.readRegister8(LIS2HH12_TEMP_H)<<8) + lis.readRegister8(LIS2HH12_TEMP_L));
  startTime = micros();
  uint8_t rgb[3];
  uint8_t LEDNO;

  // if there is a mvmt interrupt
  if(mvmtInterrupt){
    //SerialUSB.println(millis());
    if(millis() > lastMvmt + 2000)
      stringIndex = 0;
    else
      stringIndex = (stringIndex + 1)%4;
    SerialUSB.println(stringIndex);
    //bgfx.pattern_bitmap(FIRSTLED, LEDCOUNT-1);
    lastMvmt = millis();
    bgfx.pattern_fill(BL_BLACK, FIRSTLED, LEDCOUNT-1);
    bgfx.writeBrightnessArray();
    delay(15);
    if(lis.readRegister8(LIS2HH12_IG_SRC1) & 0b0100)
      for(int i = 0; i < 8*message[stringIndex].length(); i++){
        bgfx.pattern_text(i, 10, message[stringIndex]);
        bgfx.writeBrightnessArrayQuick();
      }
    else
      for(int i = 8*message[stringIndex].length(); i > 0 ; i--){
        bgfx.pattern_text(i, 10, message[stringIndex]);
        bgfx.writeBrightnessArrayQuick();
      }
    bgfx.pattern_fill(BL_BLACK, FIRSTLED, LEDCOUNT-1);
    bgfx.writeBrightnessArray();
    delay(15);
    mvmtInterrupt = false;
  }
  
  // keep watching the push button:
  button.tick();

  switch(FSM){
  case(FSM_DISPLAY):
    updateImage(0);
  break;

  case(FSM_SHOW_PARAM_COLOR):
    bgfx.pattern_fill(state_ctrl*2, FIRSTLED, LEDCOUNT-1);
  break;

  case(FSM_CTRL_COLOR):
    // zero everything out
    for(int i = 0; i < LEDCOUNT; i++){
      bgfx.loadSingleLedHSV(0, MAXSATURATION, 0, i);
    }
    
    // leave a gap between variants (+1)
    for(int i = 0; i < VARIANTCOUNT; i++){
      for(int j = 0; j < CUSTOMCOLORCOUNT; j++){
        bgfx.loadSingleLedColor(customColor[i][j], j + i*(CUSTOMCOLORCOUNT+1));
      }
    }
    
    // make one led blink
    // (adjust for skipped led)
    bgfx.blinkOneLED(colorSelector + (colorSelector/3));
    

  break;

  case(FSM_CTRL_SPEED): 
    updateImage(5);
  break;

  case(FSM_CTRL_LENGTH):
    updateImage(5);
  break;

  }
  
  //lis.readAccel(&x, &y, &z); 
  scale();
  //SerialUSB.println(x2)
  motionBrightness = abs(x2);
  if(motionBrightness>1)
    motionBrightness = 1;
  bgfx.writeBrightnessArray();

  
  /* wait until framerate as period (seconds) is elapsed
  double periodInSeconds;
  do{
    periodInSeconds = 1000000.0 / (micros() - startTime);
  }while(periodInSeconds < FRAMEPERIOD_SECONDS);
  
  //SerialUSB.print("freq: ");
  //SerialUSB.print(1000000.0 / (micros() - startTime));
  //SerialUSB.println(" Hz");*/
}


#define FSM_DISPLAY 0
#define FSM_SHOW_PARAM_COLOR 1
#define FSM_CTRL_COLOR 2
#define FSM_CTRL_SPEED 3
#define FSM_CTRL_LENGTH 4


// this function will be called when the button was pressed 1 time and them some time has passed.
void myClickFunction() {

  //SerialUSB.println("CLICK");
  
  switch(FSM){
  case(FSM_DISPLAY):
    state_display = (state_display + 1) % STATE_COUNT;
    phase = 0;
    newState = true;
  break;

  case(FSM_SHOW_PARAM_COLOR):
    // should be impossible. return to regular display
    FSM = FSM_DISPLAY;
    button.purgatory();

  case(FSM_CTRL_COLOR):
  // from color state_display, short press changes color
  customColor[colorSelector/3][colorSelector%3] = (customColor[colorSelector/3][colorSelector%3]+1) % UNIQUECOLORS;
  break;
  
  case(FSM_CTRL_SPEED):
  // change speed_tau
  speed_tau = speed_tau + .1;
  if(speed_tau > 1)
    speed_tau = .1;
  break;
  
  
  case(FSM_CTRL_LENGTH):
  // change length_tau
  length_tau = length_tau + .1;
  if(length_tau > 1)
    length_tau = 0;
  break;
  }
} // myClickFunction


// this function will be called when the button was pressed 2 times in a short timeframe.
void myDoubleClickFunction() {
  SerialUSB.println("DOUBLE CLICK");
  if(FSM == FSM_DISPLAY){
    // undo state_display change from single click
    state_display = (state_display - 1) % STATE_COUNT;
    
    brightness = (brightness + .1);
    if(brightness >= 0.6)
      brightness = .12;
    bgfx.setBrightness(brightness);
  }
} // myDoubleClickFunction


#define STATE_MENU_COLOR 0 
#define STATE_MENU_SPEED 1
#define STATE_MENU_LENGTH 2

// this function will be called when the button was pressed 1 time and them some time has passed.
// long press corresponds to color selector
// display the color selector frame, but only move to that FSM on release (STOP function)
void myLongPressStartFunction() {

  SerialUSB.println("LONG START");
  switch(FSM){
  case(FSM_DISPLAY):
  // if coming from display FSM
  FSM = FSM_SHOW_PARAM_COLOR;
  state_ctrl = STATE_MENU_COLOR;
  break;

  case(FSM_SHOW_PARAM_COLOR):
  // should be impossible. return to regular display
  FSM = FSM_DISPLAY;
  button.purgatory();

  case(FSM_CTRL_COLOR):
    // from color state_display, long press changes color location
    colorSelector = (colorSelector + 1)%9;
  break;
  
  case(FSM_CTRL_SPEED):
  // exit mode
  FSM = FSM_DISPLAY;
  button.purgatory();
  
  case(FSM_CTRL_LENGTH):
  // exit mode
  FSM = FSM_DISPLAY;
  button.purgatory();
  break;
  }
}

void myLongPressStopFunction() {
  SerialUSB.println("LONG STOP");
  // move to the actual control FSM if coming from display
  if(FSM == FSM_SHOW_PARAM_COLOR){
    FSM = FSM_CTRL_COLOR;
    colorSelector = 0;
    
  }
}

// super long press corresponds to speed_tau adjustment, or return to display FSM
void mySuperLongPressStartFunction() {
  
  SerialUSB.println("S LONG START");
  // move to the actual control FSM if coming from display
  if(FSM == FSM_SHOW_PARAM_COLOR){
    state_ctrl = STATE_MENU_SPEED;
  }
  else{
    FSM = FSM_DISPLAY;
    button.purgatory();
  }
  
}

void mySuperLongPressStopFunction() {
  SerialUSB.println("S LONG STOP");
  // move to speed_tau control if coming from display
  if(FSM == FSM_SHOW_PARAM_COLOR){
    FSM = FSM_CTRL_SPEED;
  }
  else{
    
    // end blink
    bgfx.blinkOff();
    
    FSM = FSM_DISPLAY;
    button.purgatory();
  }
}


// ultra long press corresponds to length_tau adjustment
void myUltraLongPressStartFunction() {
  SerialUSB.println("U LONG START");
  state_ctrl = STATE_MENU_LENGTH;
  button.purgatory();
} 

void myUltraLongPressStopFunction() {
  SerialUSB.println("U LONG STOP");
  FSM = FSM_CTRL_LENGTH;
}

void scale(){
  if(gravity == 2){
      x2 = (float)x*0.000244;
      y2 = (float)y*0.000244;
      z2 = (float)z*0.000244;
  }
  else if(gravity == 1){
      x2 = (float)x*0.000122;
      y2 = (float)y*0.000122;
      z2 = (float)z*0.000122;
  }
  else{
      x2 = (float)x*0.000061;
      y2 = (float)y*0.000061;
      z2 = (float)z*0.000061-1.000;    
  }  
}
