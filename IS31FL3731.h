// casually modified from Adafruit library by Julian Loiacono, Founder dragonTech LLC

#ifndef _IS31FL3731_H_
#define _IS31FL3731_H_

#include <Wire.h>
#include <Arduino.h>
#include <Adafruit_GFX.h>

#define ISSI_ADDR_DEFAULT 0x74

#define ISSI_REG_CONFIG  0x00
#define ISSI_REG_CONFIG_PICTUREMODE 0x00
#define ISSI_REG_CONFIG_AUTOPLAYMODE 0x08
#define ISSI_REG_CONFIG_AUDIOPLAYMODE 0x18

#define ISSI_REG_PICTUREFRAME  0x01

#define ISSI_REG_AUTOCONT1  0x02
#define ISSI_REG_AUTOCONT1_ENDLESS_ALLFRAMES  0x00
#define ISSI_REG_AUTOCONT2  0x03
#define ISSI_REG_AUTOCONT2_11ms  0x01
#define ISSI_REG_AUTOCONT2_44ms  0x04

#define ISSI_REG_SHUTDOWN 0x0A
#define ISSI_REG_AUDIOSYNC 0x06

#define ISSI_REG_DISPLAY 0x05
#define ISSI_COMMANDREGISTER 0xFD
#define ISSI_BANK_FUNCTIONREG 0x0B    // helpfully called 'page nine'


class IS31FL3731 : public Adafruit_GFX {
 public:
  IS31FL3731(uint8_t x=16, uint8_t y=9); 
  boolean begin(TwoWire * thisWire);
  void drawPixel(int16_t x, int16_t y, uint16_t color);
  void clear(uint8_t brightness);
  void setLEDPWM(uint8_t lednum, uint8_t pwm, uint8_t bank = 0);
  void audioSync(boolean sync);
  void setFrame(uint8_t b);
  void displayFrame(uint8_t frame);
  void writeVector(uint8_t * brightness, uint8_t startPt, uint8_t endPt);
  void writeOnMatrix(uint8_t * LED_ON);
  void writeBlinkMatrix(uint8_t * BLINK_ON);

  uint8_t readRegister8(uint8_t bank, uint8_t reg);
  void writeRegister8(uint8_t bank, uint8_t reg, uint8_t data);

  void selectBank(uint8_t bank);
  
 protected:
  uint8_t _i2caddr, _frame;
  TwoWire * _myWire;
};

class IS31FL3731_Wing : public IS31FL3731 {
 public:
  IS31FL3731_Wing(void);
  void drawPixel(int16_t x, int16_t y, uint16_t color);
};



#endif
