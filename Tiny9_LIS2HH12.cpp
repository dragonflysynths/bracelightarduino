/*
Tiny9's LIS2HH12 Library Main Source File
Tiny9_LIS2HH12.cpp

T.J. Schramm @ Tiny9
Created: Nov 14, 2017
Updated: Nov 14, 2017

Modified Bildr LIS2HH12 Source File @ 
to support I2C and SPI Communication (not working yet)

Hardware Resources:
- Arduino Development Board
- Tiny9 Triple Access Accelerometer LIS2HH12

Development Environment Specifics:
Arduino 1.6.8
Tiny9 Triple Axis Accelerometer Breakout - LIS2HH12
Arduino Uno
Arduino Nano
*/

//#include "Arduino.h"
#include "Tiny9_LIS2HH12.h"
#include <SPI.h>

#define LIS2HH12_DEVICE (0b0011110)    // Device Address for LIS2HH12
#define LIS2HH12_TO_READ (1)      // Number of Bytes Read - Two Bytes Per Axis

LIS2HH12::LIS2HH12(TwoWire * thisWire) {
	_myWire = thisWire;
	status = LIS2HH12_OK;
	error_code = LIS2HH12_NO_ERROR;
	
	gains[0] = 1;//0.00376390;		// Original gain 0.00376390 
	gains[1] = 1;//0.00376009;		// Original gain 0.00376009
	gains[2] = 1;//0.00349265;		// Original gain 0.00349265
	I2C = true;
}
int LIS2HH12::scale(int gravity) {
	if(I2C) {
		_myWire->begin(LIS2HH12_DEVICE);				// If in I2C Mode Only
		writeToI2C(LIS2HH12_CTRL4, ((gravity << 4) + 4));
	}
}
/*LIS2HH12::LIS2HH12(int CS) {
	status = LIS2HH12_OK;
	error_code = LIS2HH12_NO_ERROR;
	
	gains[0] = 0.00376390;
	gains[1] = 0.00376009;
	gains[2] = 0.00349265;
	_CS = CS;
	I2C = false;
	SPI.begin();
	SPI.setDataMode(SPI_MODE3);
	pinMode(_CS, OUTPUT);
	digitalWrite(_CS, HIGH);
}*/

void LIS2HH12::powerOn() {
  
  // force reboot the thing
  writeRegister8(LIS2HH12_CTRL6, 0b10000000);
  while(readRegister8(LIS2HH12_CTRL6)&0b10000000);
  
	//LIS2HH12 TURN ON
	writeToI2C(LIS2HH12_ACT_THS, 0);	// Activity/Inactivity detection function disabled     
	writeToI2C(LIS2HH12_ACT_DUR, 0);	// Activity/Inactivity detection function disabled	
	writeToI2C(LIS2HH12_FIFO_CTRL, 0);
  
  // no high resolution 0, frequency 100Hz 011, no data blocking 0, XYZ enabled 111
  writeRegister8(LIS2HH12_CTRL1, 0b00110111);
  
  // default for CTRL2 highpass settings
  
  // disable fifo, gen1 on int1, enable interrupt 1 on INT1 line
  writeRegister8(LIS2HH12_CTRL3, 0b00001000);
  // 100Hz anti-aliasing, 4g gravity, I2C increment auto, dont disable I2c
  writeRegister8(LIS2HH12_CTRL4, 0b10101000);
  // interrupt active high, push/pull
  writeRegister8(LIS2HH12_CTRL5, 0b00000000);
  // no interrupts on int2 (CTRL6)
  // dont force latched interrupt, no 4D
  writeRegister8(LIS2HH12_CTRL7, 0b00000000);
  
  // enable all said interrupts (X, 6D, XL, XH ... YL, YH)
  // MUST BE SET TO 01111111 OTHERWISE THIS CHINESE POS WILL NOT WORK!
  writeRegister8(LIS2HH12_IG_CFG1, 0b01111111);
  // 6d must be set to 1 apparently
  writeRegister8(LIS2HH12_IG_CFG1, 0b01001100);
  // set threshold registers to arbitrary value
  // increasing threshold seems to increase sensitivity
  writeRegister8(LIS2HH12_IG_THS_X1, 0b01100000);
  writeRegister8(LIS2HH12_IG_THS_Y1, 0b01100000);
  writeRegister8(LIS2HH12_IG_THS_Z1, 0b01100000);
  // set duration to some value, and initialize it
  writeRegister8(LIS2HH12_IG_DUR1, 0b00000000);

}


/*********************** READING ACCELERATION ***********************/
/*    Reads Acceleration into Three Variables:  x, y and z          */

void LIS2HH12::readAccel(int *xyz){
	readAccel(xyz, xyz + 1, xyz + 2);
}

void LIS2HH12::readAccel(int *x, int *y, int *z) {
	readFromI2C(40, 6, _buff);	// Read Accel Data from LIS2HH12
	
	// Each Axis @ All g Ranges: 10 Bit Resolution (2 Bytes)
	*x = (((int)_buff[1]) << 8) | _buff[0];   
	*y = (((int)_buff[3]) << 8) | _buff[2];
	*z = (((int)_buff[5]) << 8) | _buff[4];
}



uint8_t LIS2HH12::WHOAMI(){
	uint8_t retByte[1];
	readFromI2C(LIS2HH12_WHO_AM_I, 1, retByte);	// If I2C Communication
	return retByte[0];
}

void LIS2HH12::get_Gxyz(double *xyz){
	int i;
	int xyz_int[3];
	readAccel(xyz_int);
	for(i=0; i<3; i++){
		xyz[i] = xyz_int[i] * gains[i];
	}
}


/*************************** WRITE TO I2C ***************************/
/*      Start; Send Register Address; Send Value To Write; End      */
void LIS2HH12::writeToI2C(byte _address, byte _val) {
	_myWire->beginTransmission(LIS2HH12_DEVICE); 
	_myWire->write(_address);             
	_myWire->write(_val);                 
	_myWire->endTransmission();         
}


uint8_t  LIS2HH12::readRegister8(uint8_t reg) {
 uint8_t x;

 _myWire->beginTransmission(LIS2HH12_DEVICE);
 _myWire->write((byte)reg);
 _myWire->endTransmission();

 _myWire->beginTransmission(LIS2HH12_DEVICE);
 _myWire->requestFrom(LIS2HH12_DEVICE, (byte)1);
 x = _myWire->read();
 _myWire->endTransmission();

  return x;
}


void LIS2HH12::writeRegister8(uint8_t reg, uint8_t data) {

  _myWire->beginTransmission(LIS2HH12_DEVICE);
  _myWire->write((byte)reg);
  _myWire->write((byte)data);
  _myWire->endTransmission();
}


/*************************** READ FROM I2C **************************/
/*                Start; Send Address To Read; End                  */
void LIS2HH12::readFromI2C(byte address, int num, byte _buff[]) {
	_myWire->beginTransmission(LIS2HH12_DEVICE);  
	_myWire->write(address);             
	_myWire->endTransmission();         
	
	_myWire->beginTransmission(LIS2HH12_DEVICE); 
	_myWire->requestFrom(LIS2HH12_DEVICE, num);  // Request 6 Bytes
	
	int i = 0;
	while(_myWire->available())					
	{ 
		_buff[i] = _myWire->read();				// Receive Byte
		i++;
	}
	if(i != num){
		status = LIS2HH12_ERROR;
		error_code = LIS2HH12_READ_ERROR;
	}
	_myWire->endTransmission();         	
}

/*************************** SELF_TEST BIT **************************/
/*                            ~ GET & SET                           */
/*bool LIS2HH12::getSelfTestBit() {
	return getRegisterBit(LIS2HH12_DATA_FORMAT, 7);
}*/

// If Set (1) Self-Test Applied. Electrostatic Force exerted on the sensor
//  causing a shift in the output data.
// If Set (0) Self-Test Disabled.
/*void LIS2HH12::setSelfTestBit(bool selfTestBit) {
	setRegisterBit(LIS2HH12_DATA_FORMAT, 7, selfTestBit);
}*/

/*************************** SPI BIT STATE **************************/
/*                           ~ GET & SET                            */
/*bool LIS2HH12::getSpiBit() {
	return getRegisterBit(LIS2HH12_DATA_FORMAT, 6);
}*/

// If Set (1) Puts Device in 3-wire Mode
// If Set (0) Puts Device in 4-wire SPI Mode
/*void LIS2HH12::setSpiBit(bool spiBit) {
	setRegisterBit(LIS2HH12_DATA_FORMAT, 6, spiBit);
}*/

/*********************** INT_INVERT BIT STATE ***********************/
/*                           ~ GET & SET                            */
/*bool LIS2HH12::getInterruptLevelBit() {
	return getRegisterBit(LIS2HH12_DATA_FORMAT, 5);
}*/

// If Set (0) Sets the Interrupts to Active HIGH
// If Set (1) Sets the Interrupts to Active LOW
/*void LIS2HH12::setInterruptLevelBit(bool interruptLevelBit) {
	setRegisterBit(LIS2HH12_DATA_FORMAT, 5, interruptLevelBit);
}*/

/************************* FULL_RES BIT STATE ***********************/
/*                           ~ GET & SET                            */
/*bool LIS2HH12::getFullResBit() {
	return getRegisterBit(LIS2HH12_DATA_FORMAT, 3);
}*/

// If Set (1) Device is in Full Resolution Mode: Output Resolution Increase with G Range
//  Set by the Range Bits to Maintain a 4mg/LSB Scale Factor
// If Set (0) Device is in 10-bit Mode: Range Bits Determine Maximum G Range
//  And Scale Factor
/*void LIS2HH12::setFullResBit(bool fullResBit) {
	setRegisterBit(LIS2HH12_DATA_FORMAT, 3, fullResBit);
}*/

/*************************** JUSTIFY BIT STATE **************************/
/*                           ~ GET & SET                            */
/*bool LIS2HH12::getJustifyBit() {
	return getRegisterBit(LIS2HH12_DATA_FORMAT, 2);
}*/

// If Set (1) Selects the Left Justified Mode
// If Set (0) Selects Right Justified Mode with Sign Extension
/*void LIS2HH12::setJustifyBit(bool justifyBit) {
	setRegisterBit(LIS2HH12_DATA_FORMAT, 2, justifyBit);
}*/

/*********************** THRESH_TAP BYTE VALUE **********************/
/*                          ~ SET & GET                             */
// Should Set Between 0 and 255
// Scale Factor is 62.5 mg/LSB
// A Value of 0 May Result in Undesirable Behavior
/*
void LIS2HH12::setTapThreshold(int tapThreshold) {
	tapThreshold = constrain(tapThreshold,0,255);
	byte _b = byte (tapThreshold);
	writeToI2C(LIS2HH12_THRESH_TAP, _b);  
}

// Return Value Between 0 and 255
// Scale Factor is 62.5 mg/LSB/* 
int LIS2HH12::getTapThreshold() {
	byte _b;
	readFromI2C(LIS2HH12_THRESH_TAP, 1, &_b);  
	return int (_b);
}*/

// /****************** GAIN FOR EACH AXIS IN Gs / COUNT *****************/
// /*                           ~ SET & GET                            */
// /*void LIS2HH12::setAxisGains(double *_gains){
	// int i;
	// for(i = 0; i < 3; i++){
		// gains[i] = _gains[i];
	// }
// }*/
// /*void LIS2HH12::getAxisGains(double *_gains){
	// int i;
	// for(i = 0; i < 3; i++){
		// _gains[i] = gains[i];
	// }
// }*/

// /********************* OFSX, OFSY and OFSZ BYTES ********************/
// /*                           ~ SET & GET                            */
// // OFSX, OFSY and OFSZ: User Offset Adjustments in Twos Complement Format
// // Scale Factor of 15.6mg/LSB
// /*void LIS2HH12::setAxisOffset(int x, int y, int z) {
	// writeToI2C(LIS2HH12_OFSX, byte (x));  
	// writeToI2C(LIS2HH12_OFSY, byte (y));  
	// writeToI2C(LIS2HH12_OFSZ, byte (z));  
// }*/

// /*void LIS2HH12::getAxisOffset(int* x, int* y, int*z) {
	// byte _b;
	// readFromI2C(LIS2HH12_OFSX, 1, &_b);  
	// *x = int (_b);
	// readFromI2C(LIS2HH12_OFSY, 1, &_b);  
	// *y = int (_b);
	// readFromI2C(LIS2HH12_OFSZ, 1, &_b);  
	// *z = int (_b);
// }*/

// /****************************** DUR BYTE ****************************/
// /*                           ~ SET & GET                            */
// // DUR Byte: Contains an Unsigned Time Value Representing the Max Time 
// //  that an Event must be Above the THRESH_TAP Threshold to qualify 
// //  as a Tap Event
// // The scale factor is 625µs/LSB
// // Value of 0 Disables the Tap/Double Tap Funcitons. Max value is 255.
// /*void LIS2HH12::setTapDuration(int tapDuration) {
	// tapDuration = constrain(tapDuration,0,255);
	// byte _b = byte (tapDuration);
	// writeToI2C(LIS2HH12_DUR, _b);  
// }*/

// /*int LIS2HH12::getTapDuration() {
	// byte _b;
	// readFromI2C(LIS2HH12_DUR, 1, &_b);  
	// return int (_b);
// }*/

// /************************** LATENT REGISTER *************************/
// /*                           ~ SET & GET                            */
// // Contains Unsigned Time Value Representing the Wait Time from the Detection
// //  of a Tap Event to the Start of the Time Window (defined by the Window 
// //  Register) during which a possible Second Tap Even can be Detected.
// // Scale Factor is 1.25ms/LSB. 
// // A Value of 0 Disables the Double Tap Function.
// // It Accepts a Maximum Value of 255.
// /*void LIS2HH12::setDoubleTapLatency(int doubleTapLatency) {
	// byte _b = byte (doubleTapLatency);
	// writeToI2C(LIS2HH12_LATENT, _b);  
// }*/

// /*int LIS2HH12::getDoubleTapLatency() {
	// byte _b;
	// readFromI2C(LIS2HH12_LATENT, 1, &_b);  
	// return int (_b);
// }*/

// /************************** WINDOW REGISTER *************************/
// /*                           ~ SET & GET                            */
// // Contains an Unsigned Time Value Representing the Amount of Time 
// //  After the Expiration of the Latency Time (determined by Latent register)
// //  During which a Second Valid Tape can Begin. 
// // Scale Factor is 1.25ms/LSB. 
// // Value of 0 Disables the Double Tap Function. 
// // It Accepts a Maximum Value of 255.
// /*void LIS2HH12::setDoubleTapWindow(int doubleTapWindow) {
	// doubleTapWindow = constrain(doubleTapWindow,0,255);
	// byte _b = byte (doubleTapWindow);
	// writeToI2C(LIS2HH12_WINDOW, _b);  
// }*/

// /*int LIS2HH12::getDoubleTapWindow() {
	// byte _b;
	// readFromI2C(LIS2HH12_WINDOW, 1, &_b);  
	// return int (_b);
// }*/

// /*********************** THRESH_ACT REGISTER ************************/
// /*                          ~ SET & GET                             */
// // Holds the Threshold Value for Detecting Activity.
// // Data Format is Unsigned, so the Magnitude of the Activity Event is Compared 
// //  with the Value is Compared with the Value in the THRESH_ACT Register. 
// // The Scale Factor is 62.5mg/LSB. 
// // Value of 0 may Result in Undesirable Behavior if the Activity Interrupt Enabled. 
// // It Accepts a Maximum Value of 255.
// /*void LIS2HH12::setActivityThreshold(int activityThreshold) {
	// activityThreshold = constrain(activityThreshold,0,255);
	// byte _b = byte (activityThreshold);
	// writeToI2C(LIS2HH12_THRESH_ACT, _b);  
// }*/

// // Gets the THRESH_ACT byte
// /*int LIS2HH12::getActivityThreshold() {
	// byte _b;
	// readFromI2C(LIS2HH12_THRESH_ACT, 1, &_b);  
	// return int (_b);
// }*/

// /********************** THRESH_INACT REGISTER ***********************/
// /*                          ~ SET & GET                             */
// // Holds the Threshold Value for Detecting Inactivity.
// // The Data Format is Unsigned, so the Magnitude of the INactivity Event is 
// //  Compared with the value in the THRESH_INACT Register. 
// // Scale Factor is 62.5mg/LSB. 
// // Value of 0 May Result in Undesirable Behavior if the Inactivity Interrupt Enabled. 
// // It Accepts a Maximum Value of 255.
// /*void LIS2HH12::setInactivityThreshold(int inactivityThreshold) {
	// inactivityThreshold = constrain(inactivityThreshold,0,255);
	// byte _b = byte (inactivityThreshold);
	// writeToI2C(LIS2HH12_THRESH_INACT, _b);  
// }*/

// /*int LIS2HH12::getInactivityThreshold() {
	// byte _b;
	// readFromI2C(LIS2HH12_THRESH_INACT, 1, &_b);  
	// return int (_b);
// }*/

// /*********************** TIME_INACT RESIGER *************************/
// /*                          ~ SET & GET                             */
// // Contains an Unsigned Time Value Representing the Amount of Time that
// //  Acceleration must be Less Than the Value in the THRESH_INACT Register
// //  for Inactivity to be Declared. 
// // Uses Filtered Output Data* unlike other Interrupt Functions
// // Scale Factor is 1sec/LSB. 
// // Value Must Be Between 0 and 255. 
// void LIS2HH12::setTimeInactivity(int timeInactivity) {
	// timeInactivity = constrain(timeInactivity,0,255);
	// byte _b = byte (timeInactivity);
	// writeToI2C(LIS2HH12_TIME_INACT, _b);  
// }

// int LIS2HH12::getTimeInactivity() {
	// byte _b;
	// readFromI2C(LIS2HH12_TIME_INACT, 1, &_b);  
	// return int (_b);
// }

// ********************** THRESH_FF Register *************************/
                         // ~ SET & GET                            
// // Holds the Threshold Value, in Unsigned Format, for Free-Fall Detection
// // The Acceleration on all Axes is Compared with the Value in THRES_FF to
// //  Determine if a Free-Fall Event Occurred. 
// // Scale Factor is 62.5mg/LSB. 
// // Value of 0 May Result in Undesirable Behavior if the Free-Fall interrupt Enabled.
// // Accepts a Maximum Value of 255.
// void LIS2HH12::setFreeFallThreshold(int freeFallThreshold) {
	// freeFallThreshold = constrain(freeFallThreshold,0,255);
	// byte _b = byte (freeFallThreshold);
	// writeToI2C(LIS2HH12_THRESH_FF, _b);  
// }

// int LIS2HH12::getFreeFallThreshold() {
	// byte _b;
	// readFromI2C(LIS2HH12_THRESH_FF, 1, &_b);  
	// return int (_b);
// }

//*********************** TIME_FF Register **************************/
//                         ~ SET & GET                            
// Stores an Unsigned Time Value Representing the Minimum Time that the Value 
//  of all Axes must be Less Than THRES_FF to Generate a Free-Fall Interrupt.
// Scale Factor is 5ms/LSB. 
// Value of 0 May Result in Undesirable Behavior if the Free-Fall Interrupt Enabled.
// Accepts a Maximum Value of 255.
// void LIS2HH12::setFreeFallDuration(int freeFallDuration) {
	// freeFallDuration = constrain(freeFallDuration,0,255);  
	// byte _b = byte (freeFallDuration);
	// writeToI2C(LIS2HH12_TIME_FF, _b);  
// }

// int LIS2HH12::getFreeFallDuration() {
	// byte _b;
	// readFromI2C(LIS2HH12_TIME_FF, 1, &_b);  
	// return int (_b);
// }

// ************************* ACTIVITY BITS ***************************/
                                                                
// bool LIS2HH12::isActivityXEnabled() {  
	// return getRegisterBit(LIS2HH12_ACT_INACT_CTL, 6); 
// }
// bool LIS2HH12::isActivityYEnabled() {  
	// return getRegisterBit(LIS2HH12_ACT_INACT_CTL, 5); 
// }
// bool LIS2HH12::isActivityZEnabled() {  
	// return getRegisterBit(LIS2HH12_ACT_INACT_CTL, 4); 
// }
// bool LIS2HH12::isInactivityXEnabled() {  
	// return getRegisterBit(LIS2HH12_ACT_INACT_CTL, 2); 
// }
// bool LIS2HH12::isInactivityYEnabled() {  
	// return getRegisterBit(LIS2HH12_ACT_INACT_CTL, 1); 
// }
// bool LIS2HH12::isInactivityZEnabled() {  
	// return getRegisterBit(LIS2HH12_ACT_INACT_CTL, 0); 
// }

// void LIS2HH12::setActivityX(bool state) {  
	// setRegisterBit(LIS2HH12_ACT_INACT_CTL, 6, state); 
// }
// void LIS2HH12::setActivityY(bool state) {  
	// setRegisterBit(LIS2HH12_ACT_INACT_CTL, 5, state); 
// }
// void LIS2HH12::setActivityZ(bool state) {  
	// setRegisterBit(LIS2HH12_ACT_INACT_CTL, 4, state); 
// }
// void LIS2HH12::setActivityXYZ(bool stateX, bool stateY, bool stateZ) {
	// setActivityX(stateX);
	// setActivityY(stateY);
	// setActivityZ(stateZ);
// }
// void LIS2HH12::setInactivityX(bool state) {  
	// setRegisterBit(LIS2HH12_ACT_INACT_CTL, 2, state); 
// }
// void LIS2HH12::setInactivityY(bool state) {  
	// setRegisterBit(LIS2HH12_ACT_INACT_CTL, 1, state); 
// }
// void LIS2HH12::setInactivityZ(bool state) {  
	// setRegisterBit(LIS2HH12_ACT_INACT_CTL, 0, state); 
// }
// void LIS2HH12::setInactivityXYZ(bool stateX, bool stateY, bool stateZ) {
	// setInactivityX(stateX);
	// setInactivityY(stateY);
	// setInactivityZ(stateZ);
// }

// bool LIS2HH12::isActivityAc() { 
	// return getRegisterBit(LIS2HH12_ACT_INACT_CTL, 7); 
// }
// bool LIS2HH12::isInactivityAc(){ 
	// return getRegisterBit(LIS2HH12_ACT_INACT_CTL, 3); 
// }

// void LIS2HH12::setActivityAc(bool state) {  
	// setRegisterBit(LIS2HH12_ACT_INACT_CTL, 7, state); 
// }
// void LIS2HH12::setInactivityAc(bool state) {  
	// setRegisterBit(LIS2HH12_ACT_INACT_CTL, 3, state); 
// }

// ************************ SUPPRESS BITS ****************************/
                                                                
// bool LIS2HH12::getSuppressBit(){ 
	// return getRegisterBit(LIS2HH12_TAP_AXES, 3); 
// }
// void LIS2HH12::setSuppressBit(bool state) {  
	// setRegisterBit(LIS2HH12_TAP_AXES, 3, state); 
// }

// *************************** TAP BITS ******************************/
                                                                
// bool LIS2HH12::isTapDetectionOnX(){ 
	// return getRegisterBit(LIS2HH12_TAP_AXES, 2); 
// }
// void LIS2HH12::setTapDetectionOnX(bool state) {  
	// setRegisterBit(LIS2HH12_TAP_AXES, 2, state); 
// }
// bool LIS2HH12::isTapDetectionOnY(){ 
	// return getRegisterBit(LIS2HH12_TAP_AXES, 1); 
// }
// void LIS2HH12::setTapDetectionOnY(bool state) {  
	// setRegisterBit(LIS2HH12_TAP_AXES, 1, state); 
// }
// bool LIS2HH12::isTapDetectionOnZ(){ 
	// return getRegisterBit(LIS2HH12_TAP_AXES, 0); 
// }
// void LIS2HH12::setTapDetectionOnZ(bool state) {  
	// setRegisterBit(LIS2HH12_TAP_AXES, 0, state); 
// }

// void LIS2HH12::setTapDetectionOnXYZ(bool stateX, bool stateY, bool stateZ) {
	// setTapDetectionOnX(stateX);
	// setTapDetectionOnY(stateY);
	// setTapDetectionOnZ(stateZ);
// }

// bool LIS2HH12::isActivitySourceOnX(){ 
	// return getRegisterBit(LIS2HH12_ACT_TAP_STATUS, 6); 
// }
// bool LIS2HH12::isActivitySourceOnY(){ 
	// return getRegisterBit(LIS2HH12_ACT_TAP_STATUS, 5); 
// }
// bool LIS2HH12::isActivitySourceOnZ(){ 
	// return getRegisterBit(LIS2HH12_ACT_TAP_STATUS, 4); 
// }

// bool LIS2HH12::isTapSourceOnX(){ 
	// return getRegisterBit(LIS2HH12_ACT_TAP_STATUS, 2); 
// }
// bool LIS2HH12::isTapSourceOnY(){ 
	// return getRegisterBit(LIS2HH12_ACT_TAP_STATUS, 1); 
// }
// bool LIS2HH12::isTapSourceOnZ(){ 
	// return getRegisterBit(LIS2HH12_ACT_TAP_STATUS, 0); 
// }

//************************** ASLEEP BIT *****************************/
                                                                
//bool LIS2HH12::isAsleep(){ 
//	return getRegisterBit(LIS2HH12_ACT_TAP_STATUS, 3); 
//}

//************************* LOW POWER BIT ***************************/
/*                                                                 
bool LIS2HH12::isLowPower(){ 
	return getRegisterBit(LIS2HH12_BW_RATE, 4); 
}
void LIS2HH12::setLowPower(bool state) {  
	setRegisterBit(LIS2HH12_BW_RATE, 4, state); 
}

/*************************** RATE BITS ******************************/
/*                                                                 
double LIS2HH12::getRate(){
	byte _b;
	readFromI2C(LIS2HH12_BW_RATE, 1, &_b);
	_b &= B00001111;
	return (pow(2,((int) _b)-6)) * 6.25;
}

void LIS2HH12::setRate(double rate){
	byte _b,_s;
	int v = (int) (rate / 6.25);
	int r = 0;
	while (v >>= 1)
	{
		r++;
	}
	if (r <= 9) { 
		readFromI2C(LIS2HH12_BW_RATE, 1, &_b);
		_s = (byte) (r + 6) | (_b & B11110000);
		writeToI2C(LIS2HH12_BW_RATE, _s);
	}
}

/*************************** BANDWIDTH ******************************/
/*                          ~ SET & GET                            
void LIS2HH12::set_bw(byte bw_code){
	if((bw_code < LIS2HH12_BW_0_05) || (bw_code > LIS2HH12_BW_1600)){
		status = false;
		error_code = LIS2HH12_BAD_ARG;
	}
	else{
		writeToI2C(LIS2HH12_BW_RATE, bw_code);
	}
}

byte LIS2HH12::get_bw_code(){
	byte bw_code;
	readFromI2C(LIS2HH12_BW_RATE, 1, &bw_code);
	return bw_code;
}




/************************* TRIGGER CHECK  ***************************/
/*                                                                 
// Check if Action was Triggered in Interrupts
// Example triggered(interrupts, LIS2HH12_SINGLE_TAP);
bool LIS2HH12::triggered(byte interrupts, int mask){
	return ((interrupts >> mask) & 1);
}

/*
 LIS2HH12_DATA_READY
 LIS2HH12_SINGLE_TAP
 LIS2HH12_DOUBLE_TAP
 LIS2HH12_ACTIVITY
 LIS2HH12_INACTIVITY
 LIS2HH12_FREE_FALL
 LIS2HH12_WATERMARK
 LIS2HH12_OVERRUNY



byte LIS2HH12::getInterruptSource() {
	byte _b;
	readFromI2C(LIS2HH12_INT_SOURCE, 1, &_b);
	return _b;
}

bool LIS2HH12::getInterruptSource(byte interruptBit) {
	return getRegisterBit(LIS2HH12_INT_SOURCE,interruptBit);
}

bool LIS2HH12::getInterruptMapping(byte interruptBit) {
	return getRegisterBit(LIS2HH12_INT_MAP,interruptBit);
}

/*********************** INTERRUPT MAPPING **************************/
 //        Set the Mapping of an Interrupt to pin1 or pin2         
// eg: setInterruptMapping(LIS2HH12_INT_DOUBLE_TAP_BIT,LIS2HH12_INT2_PIN);
/*void LIS2HH12::setInterruptMapping(byte interruptBit, bool interruptPin) {
	setRegisterBit(LIS2HH12_INT_MAP, interruptBit, interruptPin);
}

void LIS2HH12::setImportantInterruptMapping(int single_tap, int double_tap, int free_fall, int activity, int inactivity) {
	if(single_tap == 1) {
		setInterruptMapping( LIS2HH12_INT_SINGLE_TAP_BIT,   LIS2HH12_INT1_PIN );}
	else if(single_tap == 2) {
		setInterruptMapping( LIS2HH12_INT_SINGLE_TAP_BIT,   LIS2HH12_INT2_PIN );}

	if(double_tap == 1) {
		setInterruptMapping( LIS2HH12_INT_DOUBLE_TAP_BIT,   LIS2HH12_INT1_PIN );}
	else if(double_tap == 2) {
		setInterruptMapping( LIS2HH12_INT_DOUBLE_TAP_BIT,   LIS2HH12_INT2_PIN );}

	if(free_fall == 1) {
		setInterruptMapping( LIS2HH12_INT_FREE_FALL_BIT,   LIS2HH12_INT1_PIN );}
	else if(free_fall == 2) {
		setInterruptMapping( LIS2HH12_INT_FREE_FALL_BIT,   LIS2HH12_INT2_PIN );}

	if(activity == 1) {
		setInterruptMapping( LIS2HH12_INT_ACTIVITY_BIT,   LIS2HH12_INT1_PIN );}
	else if(activity == 2) {
		setInterruptMapping( LIS2HH12_INT_ACTIVITY_BIT,   LIS2HH12_INT2_PIN );}

	if(inactivity == 1) {
		setInterruptMapping( LIS2HH12_INT_INACTIVITY_BIT,   LIS2HH12_INT1_PIN );}
	else if(inactivity == 2) {
		setInterruptMapping( LIS2HH12_INT_INACTIVITY_BIT,   LIS2HH12_INT2_PIN );}
}

bool LIS2HH12::isInterruptEnabled(byte interruptBit) {
	return getRegisterBit(LIS2HH12_INT_ENABLE,interruptBit);
}

void LIS2HH12::setInterrupt(byte interruptBit, bool state) {
	setRegisterBit(LIS2HH12_INT_ENABLE, interruptBit, state);
}

void LIS2HH12::singleTapINT(bool status) {
	if(status) {
		setInterrupt( LIS2HH12_INT_SINGLE_TAP_BIT, 1);
	}
	else {
		setInterrupt( LIS2HH12_INT_SINGLE_TAP_BIT, 0);
	}
}
void LIS2HH12::doubleTapINT(bool status) {
	if(status) {
		setInterrupt( LIS2HH12_INT_DOUBLE_TAP_BIT, 1);
	}
	else {
		setInterrupt( LIS2HH12_INT_DOUBLE_TAP_BIT, 0);		
	}	
}
void LIS2HH12::FreeFallINT(bool status) {
	if(status) {
		setInterrupt( LIS2HH12_INT_FREE_FALL_BIT,  1);
	}
	else {
		setInterrupt( LIS2HH12_INT_FREE_FALL_BIT,  0);
	}	
}
void LIS2HH12::ActivityINT(bool status) {
	if(status) {
		setInterrupt( LIS2HH12_INT_ACTIVITY_BIT,   1);
	}
	else {
		setInterrupt( LIS2HH12_INT_ACTIVITY_BIT,   0);
	}
}
void LIS2HH12::InactivityINT(bool status) {
	if(status) {
		setInterrupt( LIS2HH12_INT_INACTIVITY_BIT, 1);
	}
	else {
		setInterrupt( LIS2HH12_INT_INACTIVITY_BIT, 0);
	}
}

void LIS2HH12::setRegisterBit(byte regAdress, int bitPos, bool state) {
	byte _b;
	readFromI2C(regAdress, 1, &_b);
	if (state) {
		_b |= (1 << bitPos);  // Forces nth Bit of _b to 1. Other Bits Unchanged.  
	} 
	else {
		_b &= ~(1 << bitPos); // Forces nth Bit of _b to 0. Other Bits Unchanged.
	}
	writeToI2C(regAdress, _b);  
}

bool LIS2HH12::getRegisterBit(byte regAdress, int bitPos) {
	byte _b;
	readFromI2C(regAdress, 1, &_b);
	return ((_b >> bitPos) & 1);
}*/

/********************************************************************/
                                                                 
// Print Register Values to Serial Output =
// Can be used to Manually Check the Current Configuration of Device
/*void LIS2HH12::printAllRegister() {
	byte _b;
	Serial.print("0x00: ");
	readFromI2C(0x00, 1, &_b);
	print_byte(_b);
	Serial.println("");
	int i;
	for (i=29;i<=57;i++){
		Serial.print("0x");
		Serial.print(i, HEX);
		Serial.print(": ");
		readFromI2C(i, 1, &_b);
		print_byte(_b);
		Serial.println("");    
	}
}

void print_byte(byte val){
	int i;
	Serial.print("B");
	for(i=7; i>=0; i--){
		Serial.print(val >> i & 1, BIN);
	}
} */
