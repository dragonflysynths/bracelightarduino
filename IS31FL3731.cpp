// casually modified from Adafruit library by Julian Loiacono, Founder dragonTech LLC


#include <Arduino.h>
#include "IS31FL3731.h"

#ifndef _swap_int16_t
#define _swap_int16_t(a, b) { int16_t t = a; a = b; b = t; }
#endif


/* Constructor */
IS31FL3731::IS31FL3731(uint8_t x, uint8_t y) : Adafruit_GFX(x, y) {
}

IS31FL3731_Wing::IS31FL3731_Wing(void) : IS31FL3731(15, 7) {
}



boolean IS31FL3731::begin(TwoWire * thisWire) {
  _myWire = thisWire;
  _i2caddr = ISSI_ADDR_DEFAULT;
  _frame = 0;


  // shutdown
  writeRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_SHUTDOWN, 0x00);

  delay(10);

  // out of shutdown
  writeRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_SHUTDOWN, 0x01);

  // turn on blink, tau = 1*.27s
  writeRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_DISPLAY, 1<<3 | 1);
  //writeRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_DISPLAY, 0);
  
  // picture mode
  writeRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_CONFIG, ISSI_REG_CONFIG_PICTUREMODE);

  // fade in
  //writeRegister8(ISSI_BANK_FUNCTIONREG, 0x08, 1<<4);

  displayFrame(_frame);

  // all LEDs on & 0 PWM
  clear(0); // set each led to 0 PWM

  audioSync(false);

  return true;

}


void IS31FL3731::clear(uint8_t brightness) {
  // all LEDs on & 0 PWM

  selectBank(_frame);

  for (uint8_t i=0; i<6; i++) {
    _myWire->beginTransmission(_i2caddr);
    _myWire->write((byte) 0x24+i*24);
    // write 24 bytes at once
    for (uint8_t j=0; j<24; j++) {
      _myWire->write(brightness);
    }
    _myWire->endTransmission();
  }
}


void IS31FL3731::writeOnMatrix(uint8_t * LED_ON) {
  selectBank(_frame);
  _myWire->beginTransmission(_i2caddr);
  _myWire->write((byte) 0);
    for (uint8_t i=0; i<0x12; i++)
      _myWire->write((byte) LED_ON[i]);     // each 8 LEDs on
  _myWire->endTransmission(); 
}

void IS31FL3731::writeBlinkMatrix(uint8_t * BLINK_ON) {

  selectBank(_frame);
  // turn blink off
  _myWire->beginTransmission(_i2caddr);
  _myWire->write((byte) 0x12);
    for (uint8_t i=0; i<0x12; i++)
      _myWire->write((byte) BLINK_ON[i]);     // turn blink off
  _myWire->endTransmission();

}

void IS31FL3731::writeVector(uint8_t * brightness, uint8_t startPt, uint8_t endPt) {
  // all LEDs on & 0 PWM
  selectBank(_frame);
  _myWire->beginTransmission(_i2caddr);
  _myWire->write((byte) 0x24 + startPt);
  // write 144 bytes at once
  // abbreviate to 107
  for (uint8_t j=0; j<endPt; j++) {
    _myWire->write(brightness[j]);
  }
  _myWire->endTransmission();

}




void IS31FL3731::setLEDPWM(uint8_t lednum, uint8_t pwm, uint8_t bank) {
  if (lednum >= 144) return;
  writeRegister8(bank, 0x24+lednum, pwm);
}


void IS31FL3731_Wing::drawPixel(int16_t x, int16_t y, uint16_t color) {
 // check rotation, move pixel around if necessary
  switch (getRotation()) {
  case 1:
    _swap_int16_t(x, y);
    x = 15 - x - 1;
    break;
  case 2:
    x = 15 - x - 1;
    y = 7 - y - 1;
    break;
  case 3:
    _swap_int16_t(x, y);
    y = 9 - y - 1;
    break;
  }

  // charlie wing is smaller:
  if ((x < 0) || (x >= 16) || (y < 0) || (y >= 7)) return;

  if (x > 7) {
    x=15-x;
    y += 8;
  } else {
    y = 7-y;
  }

  _swap_int16_t(x, y);
 
  if (color > 255) color = 255; // PWM 8bit max

  setLEDPWM(x + y*16, color, _frame);
  return;
}



void IS31FL3731::drawPixel(int16_t x, int16_t y, uint16_t color) {
 // check rotation, move pixel around if necessary
  switch (getRotation()) {
  case 1:
    _swap_int16_t(x, y);
    x = 16 - x - 1;
    break;
  case 2:
    x = 16 - x - 1;
    y = 9 - y - 1;
    break;
  case 3:
    _swap_int16_t(x, y);
    y = 9 - y - 1;
    break;
  }

  if ((x < 0) || (x >= 16)) return;
  if ((y < 0) || (y >= 9)) return;
  if (color > 255) color = 255; // PWM 8bit max

  setLEDPWM(x + y*16, color, _frame);
  return;
}

void IS31FL3731::setFrame(uint8_t f) {
  _frame = f;
}

void IS31FL3731::displayFrame(uint8_t f) {
  if (f > 7) f = 0;
  writeRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_PICTUREFRAME, f);
}


void IS31FL3731::selectBank(uint8_t b) {
 _myWire->beginTransmission(_i2caddr);
 _myWire->write((byte)ISSI_COMMANDREGISTER);
 _myWire->write(b);
 _myWire->endTransmission();
}

void IS31FL3731::audioSync(boolean sync) {
  if (sync) {
    writeRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_AUDIOSYNC, 0x1);
  } else {
    writeRegister8(ISSI_BANK_FUNCTIONREG, ISSI_REG_AUDIOSYNC, 0x0);
  }
}

/*************/
void IS31FL3731::writeRegister8(uint8_t b, uint8_t reg, uint8_t data) {
  selectBank(b);

  _myWire->beginTransmission(_i2caddr);
  _myWire->write((byte)reg);
  _myWire->write((byte)data);
  _myWire->endTransmission();
  //Serial.print("$"); Serial.print(reg, HEX);
  //Serial.print(" = 0x"); Serial.println(data, HEX);
}

uint8_t  IS31FL3731::readRegister8(uint8_t bank, uint8_t reg) {
 uint8_t x;

 selectBank(bank);

 _myWire->beginTransmission(_i2caddr);
 _myWire->write((byte)reg);
 _myWire->endTransmission();

 _myWire->beginTransmission(_i2caddr);
 _myWire->requestFrom(_i2caddr, (byte)1);
 x = _myWire->read();
 _myWire->endTransmission();

// Serial.print("$"); Serial.print(reg, HEX);
//  Serial.print(": 0x"); Serial.println(x, HEX);

  return x;
}
