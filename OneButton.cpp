// -----
// OneButton.cpp - Library for detecting button clicks, doubleclicks and long press pattern on a single button.
// This class is implemented for use with the Arduino environment.
// Copyright (c) by Matthias Hertel, http://www.mathertel.de
// This work is licensed under a BSD style license. See http://www.mathertel.de/License.aspx
// More information on: http://www.mathertel.de/Arduino
// -----
// Changelog: see OneButton.h
// -----

// Modified by Julian Loiacono, Founder dragonTech LLC, 8/21/18

#include "OneButton.h"

// ----- Initialization and Default Values -----

OneButton::OneButton(int pin, int activeLow)
{
  _pin = pin;

  _debounceTicks = 50;      // number of millisec that have to pass by before a click is assumed as safe.
  _clickTicks = 300;        // number of millisec that have to pass by before a click is detected.
  _longPressTicks = 1000;       // number of millisec that have to pass by before a long button press is detected.
  _superLongPressTicks = 2000;       // number of millisec that have to pass by before a long button press is detected.
  _ultraLongPressTicks = 3000;       // number of millisec that have to pass by before a long button press is detected.
 
  _state = 0; // starting with state 0: waiting for button to be pressed
  _isLongPressed = false;  // Keep track of long press state

  if (activeLow) {
    // the button connects the input pin to GND when pressed.
    _buttonReleased = HIGH; // notPressed
    _buttonPressed = LOW;

    // use the given pin as input and activate internal PULLUP resistor.
    pinMode( pin, INPUT_PULLUP );

  } else {
    // the button connects the input pin to VCC when pressed.
    _buttonReleased = LOW;
    _buttonPressed = HIGH;

    // use the given pin as input
    pinMode(pin, INPUT);
  } // if

  // no functions attached yet: clear all function pointers.
  _clickFunc = NULL;
  _doubleClickFunc = NULL;
  _longPressStartFunc = NULL;
  _longPressStopFunc = NULL;
  _duringLongPressFunc = NULL;
} // OneButton


// explicitly set the number of millisec that have to pass by before a click is assumed as safe.
void OneButton::setDebounceTicks(int ticks) { 
  _debounceTicks = ticks;
} // setDebounceTicks

// explicitly set the number of millisec that have to pass by before a click is detected.
void OneButton::setClickTicks(int ticks) { 
  _clickTicks = ticks;
} // setClickTicks


// explicitly set the number of millisec that have to pass by before a long button press is detected.
void OneButton::setPressTicks(int ticks) {
  _longPressTicks = ticks;
} // setPressTicks


// save function for click event
void OneButton::attachClick(callbackFunction newFunction)
{
  _clickFunc = newFunction;
} // attachClick


// save function for doubleClick event
void OneButton::attachDoubleClick(callbackFunction newFunction)
{
  _doubleClickFunc = newFunction;
} // attachDoubleClick


// save function for longPressStart event
void OneButton::attachLongPressStart(callbackFunction newFunction)
{
  _longPressStartFunc = newFunction;
} // attachLongPressStart

// save function for longPressStop event
void OneButton::attachLongPressStop(callbackFunction newFunction)
{
  _longPressStopFunc = newFunction;
} // attachLongPressStop


// save function for longPressStart event
void OneButton::attachSuperLongPressStart(callbackFunction newFunction)
{
  _superLongPressStartFunc = newFunction;
} // attachLongPressStart

// save function for longPressStop event
void OneButton::attachSuperLongPressStop(callbackFunction newFunction)
{
  _superLongPressStopFunc = newFunction;
} // attachLongPressStop


// save function for longPressStart event
void OneButton::attachUltraLongPressStart(callbackFunction newFunction)
{
  _ultraLongPressStartFunc = newFunction;
} // attachLongPressStart

// save function for longPressStop event
void OneButton::attachUltraLongPressStop(callbackFunction newFunction)
{
  _ultraLongPressStopFunc = newFunction;
} // attachLongPressStop


// save function for during longPress event
void OneButton::attachDuringLongPress(callbackFunction newFunction)
{
  _duringLongPressFunc = newFunction;
} // attachDuringLongPress

// function to get the current long pressed state
bool OneButton::isLongPressed(){
  return _isLongPressed;
}

unsigned long OneButton::longPressedTime(){
  return millis() - _startTime;
}

#define STATE_IDLE 0
#define STATE_FIRSTPRESS 1
#define STATE_LONGPRESS 2
#define STATE_SUPERLONGPRESS 3
#define STATE_ULTRALONGPRESS 4
#define STATE_PURGATORY 5

void OneButton::purgatory(){
	_state = STATE_PURGATORY;
}

void OneButton::tick(void)
{
	// Detect the input information 
	int buttonLevel = digitalRead(_pin); // current button signal.
	unsigned long now = millis(); // current (relative) time in msecs.

	// Implementation of the state machine
	switch(_state){
		case STATE_IDLE: // waiting for menu pin being pressed.
		if (buttonLevel == _buttonPressed) {
		  _state = STATE_FIRSTPRESS; // step to state 1
		  _laststartTime = _startTime;
		  _startTime = now; // remember starting time
		} // if
		break;


		case STATE_FIRSTPRESS: // waiting for menu pin being released.
		if ((buttonLevel == _buttonReleased) && ((unsigned long)(now - _startTime) < _debounceTicks)) {
		  // button was released to quickly so I assume some debouncing.
		  // go back to state 0 without calling a function.
		  _state = STATE_IDLE;

		} else if (buttonLevel == _buttonReleased) {
		  _stopTime = now; // remember stopping time

		  // if previous click was recent, call double click
		  if(_startTime - _laststartTime < _clickTicks){
		      // this was a 2 click sequence.
		      if (_doubleClickFunc) _doubleClickFunc();
		      _state = STATE_IDLE; // restart.
		  }
		  else{ // otherwise, single click
		      // this was only a single short click
		      if (_clickFunc) _clickFunc();
		      _state = STATE_IDLE; // restart.
		  }

		} else if ((buttonLevel == _buttonPressed) && ((unsigned long)(now - _startTime) > _longPressTicks)) {
		  _isLongPressed = true;  // Keep track of long press state
		  if (_longPressStartFunc) _longPressStartFunc();
		  if (_duringLongPressFunc) _duringLongPressFunc();
		  _state = STATE_LONGPRESS; // step to state 6
		}
		break;


		case STATE_LONGPRESS: // waiting for menu pin being release after long press.
	    if (buttonLevel == _buttonReleased) {
			_isLongPressed = false;  // Keep track of long press state
			if(_longPressStopFunc) _longPressStopFunc();
			_state = STATE_IDLE; // restart.
	    } 
		else if ((buttonLevel == _buttonPressed) && ((unsigned long)(now - _startTime) > _superLongPressTicks)) {
		  _isLongPressed = true;  // Keep track of long press state
		  if (_longPressStartFunc) _superLongPressStartFunc();
		  if (_duringLongPressFunc) _duringLongPressFunc();
		  _state = STATE_SUPERLONGPRESS; // step to state 6
		}
	    else {
			// button is being long pressed
			_isLongPressed = true; // Keep track of long press state
			if (_duringLongPressFunc) _duringLongPressFunc();
	    } // if  
	    break;


		case STATE_SUPERLONGPRESS: // waiting for menu pin being release after long press.
	    if (buttonLevel == _buttonReleased) {
			_isLongPressed = false;  // Keep track of long press state
			if(_longPressStopFunc) _superLongPressStopFunc();
			_state = STATE_IDLE; // restart.
	    } 
		else if ((buttonLevel == _buttonPressed) && ((unsigned long)(now - _startTime) > _ultraLongPressTicks)) {
		  _isLongPressed = true;  // Keep track of long press state
		  if (_longPressStartFunc) _ultraLongPressStartFunc();
		  if (_duringLongPressFunc) _duringLongPressFunc();
		  _state = STATE_ULTRALONGPRESS; // step to state 6
		}
		else {
			// button is being long pressed
			_isLongPressed = true; // Keep track of long press state
			if (_duringLongPressFunc) _duringLongPressFunc();
	    } // if  
	    break;


		case STATE_ULTRALONGPRESS: // waiting for menu pin being release after long press.
	    if (buttonLevel == _buttonReleased) {
  			_isLongPressed = false;  // Keep track of long press state
  			if(_longPressStopFunc) _ultraLongPressStopFunc();
  			_state = STATE_IDLE; // restart.
	    }
	    else {
  			// button is being long pressed
  			_isLongPressed = true; // Keep track of long press state
  			if (_duringLongPressFunc) _duringLongPressFunc();
  	    } // if  ]
	    break;

	    // ignore button being held longer
		case STATE_PURGATORY: //will not leave state until button is released
		if(buttonLevel == _buttonReleased) {
			_state = STATE_IDLE; // restart.
		}
	    break;
	}

} // OneButton.tick()


// end.

