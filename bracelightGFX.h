#ifndef bracelightGFX_h
#define bracelightGFX_h

#include "IS31FL3731.h"
#define IS_SHUTDOWN_PIN 10

#define MAXSATURATION 1.0


#define BUTTONPIN 20
#define RES_DIV_GND 9
#define HALF_BV     8 
#define IR_PWR 18
#define IR_SEND 30
#define ACC_CS 12
#define ACC_INT1 6
#define ACC_INT2 7
#define IS_INTB  13

#define BL_RED    0
#define BL_ORANGE 1
#define BL_YELLOW 2
#define BL_GREEN  3
#define BL_CYAN   4
#define BL_BLUE   5
#define BL_PURPLE 6
#define BL_PINK   7
#define BL_WHITE  8
#define BL_BLACK  9

  
#define UNIQUECOLORS 10

//#define LEDCOUNT 27
#define LEDCOUNT 24
#define FIRSTLED 0
#define RED 0
#define GRN 1
#define BLU 2
#define COLORCOUNT 3

class bracelightGFX
{

public:
  // ----- Constructor -----
  bracelightGFX(void);
  void pattern_rainbow(double phase, uint8_t startled, uint8_t endled, double alpha);
  void pattern_valuefade2(double phase, uint8_t startled, uint8_t endled, double alpha, uint8_t * customColor);
  void pattern_huefade2(double phase, uint8_t startled, uint8_t endled, double alpha, uint8_t * customColor);
  void pattern_swap3(double phase, uint8_t startled, uint8_t endled, double alpha, uint8_t * customColor);
  void pattern_fill(uint8_t color, uint8_t startled, uint8_t endled);
  void pattern_text(uint8_t phase, uint8_t startled, String disptext);
  void pattern_bitmap(uint8_t startled, uint8_t endled);
  
  void loadSingleLed(uint8_t * rgb, uint8_t LEDNO);
  void loadSingleLedColor(uint8_t color, uint8_t LEDNO);
  void loadSingleLedHSV(double hue, double saturation, double value, uint8_t LEDNO);
  void allLedsOn(void);
  void blinkOneLED(uint8_t LEDNO);
  void writeBrightnessArray(void);
  void writeBrightnessArrayQuick(void);
  void writeBlinkMatrix(uint8_t * blinkPattern);
  void setBrightness(double thisbrightness);
  void blinkOff(void);
  void initialize(TwoWire * thisWire);
  void pictureMode(void);
  void autoMode(void);
  
  uint8_t rgbBlack[3] = {0,0,0};
  uint8_t rgbWhite[3] = {255,255,255};
  uint8_t rgbRed[3]   = {255,0,0};
  uint8_t rgbGreen[3] = {0,255,0};
  uint8_t rgbBlue[3]  = {0,0,255};
private:

  void hsvToRgb(double h, double s, double v, double * gamma, uint8_t * rgb);
  void colorToRgb(uint8_t color, uint8_t * rgb);
  
  uint8_t rgb[3];
  
  const double colorHue[UNIQUECOLORS] =
  {
    0,      // red    0
    1.0/16, // orange 1
    1.0/6,  // yellow 2
    1.0/3,  // green  3
    1.0/2,  // cyan   4 
    2.0/3,  // blue   5
    7.125/9,  // purple 6
    5.0/6,  // pink   7
    0,      // white  8
    0       // black  9
  };
  
  const double colorSaturation[UNIQUECOLORS]  =
  {
    1,  // red
    1,  // orange
    1,  // yellow
    1,  // green
    1,  // cyan
    1,  // blue
    1,  // purple
    1,  // pink
    0,  // white
    0   // black
  };
  
  
  const double colorValue[UNIQUECOLORS]  =
  {
    1,  // red
    1,  // orange
    1,  // yellow
    1,  // green
    1,  // cyan
    1,  // blue
    1,  // purple
    1,  // pink
    1,  // white
    0   // black
  };
  
  
  
  const uint8_t ADDR[LEDCOUNT][COLORCOUNT] = 
  {
    {0x10, 0x20, 0x30}, // D1
    {0x40, 0x50, 0x60}, // D2
    {0x00, 0x21, 0x31}, // D3
    {0x41, 0x51, 0x61}, // D4
    {0x01, 0x11, 0x32}, // D5
    {0x42, 0x52, 0x62}, // D6
    {0x02, 0x12, 0x22}, // D7
    {0x43, 0x53, 0x63}, // D8
    {0x03, 0x13, 0x23}, // D9
    {0x33, 0x54, 0x64}, // D10
    {0x04, 0x14, 0x24}, // D11
    {0x34, 0x44, 0x65}, // D12
    {0x05, 0x15, 0x25}, // D13
    {0x18, 0x28, 0x38}, // D14
    {0x48, 0x58, 0x68}, // D15
    {0x08, 0x29, 0x39}, // D16
    {0x49, 0x59, 0x69}, // D17
    {0x09, 0x19, 0x3A}, // D18
    {0x4A, 0x5A, 0x6A}, // D19
    {0x0A, 0x1A, 0x2A}, // D20
    {0x4B, 0x5B, 0x6B}, // D21
    {0x0B, 0x1B, 0x2B}, // D22
    {0x3B, 0x5C, 0x6C}, // D23
    {0x0C, 0x1C, 0x2C}, // D24
    //{0x3C, 0x4C, 0x6D}, // D25
    //{0x0D, 0x1D, 0x2D}, // D26
    //{0x3D, 0x4D, 0x5D}  // D27
  };
  
  
  const double colorAdjustment[3]{
    // account for blue being overpowered
    // also consider color-specific gammas
    1 * 255,.9 * 255,.8 * 255
  };
  
  uint8_t brightnessVector[144];
  double GAMMA[COLORCOUNT]  = {2, 2, 2};
  double brightness;
  uint8_t LED_ON[0x12];
  uint8_t LED_BLINK[0x12];
  
};
#endif
